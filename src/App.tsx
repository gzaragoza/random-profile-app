import React from "react";
import "./App.scss";
import { Home } from "./components/pages/home/Home";

function App() {
  return (
    <div className="container-fluid p-0 ">
      <Home />
    </div>
  );
}

export default App;
