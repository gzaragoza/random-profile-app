import React from "react";
import { shallow } from "enzyme";
import { Chip } from "../../../../components/common/chip/Chip";

const wrapper = shallow(<Chip keyWord={"Spain"} chipType={"delete"} />);
describe("<Chip /> component test", () => {
  test("Snapshot test", () => {
    expect(wrapper).toMatchSnapshot();
  });

  test("Chip renders ok with the right word", () => {
    expect(wrapper.find(".chipContent").text().trim()).toBe("Spain");
  });
});
