import React from "react";
import { shallow } from "enzyme";
import { Home } from "../../../../components/pages/home/Home";

const wrapper = shallow(<Home />);

describe("<Home /> component unit testing", () => {
  test("Snapshot test", () => {
    expect(wrapper).toMatchSnapshot();
  });

  test("Ensure FilterBar component is present into Home component  and has props addChipCountry and addChipName", () => {
    const FilterBar = wrapper.find("FilterBar");
    expect(FilterBar.prop("addChipName")).toBeTruthy();
    expect(FilterBar.prop("addChipCountry")).toBeTruthy();
  });

  test("Ensure UserCard components are present into Home component to display gallery of users", () => {
    const UserCard = wrapper.find("UserCard");
    expect(UserCard).toBeTruthy();
  });

  test("Ensure there is a div with ref loader at the end of page to ensure progresive rendering of gnomes gallery is working", () => {
    const DivLoaderDetector = wrapper.find("#loaderId");
    expect(DivLoaderDetector).toBeTruthy();
  });
});
