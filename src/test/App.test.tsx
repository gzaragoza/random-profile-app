import React from "react";
import { render } from "@testing-library/react";
import { shallow } from "enzyme";
import App from "../App";

const observe = jest.fn();
const wrapper = shallow(<App />);

describe("Test in main component App", () => {
  test("Snapshot test of App", () => {
    expect(wrapper).toMatchSnapshot();
  });

  test("Renders App without errors", () => {
    window.IntersectionObserver = jest.fn(function () {
      this.observe = observe;
    });

    render(<App />);
  });
});
