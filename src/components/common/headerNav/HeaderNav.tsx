import React from "react";
import "./HeaderNav.scss";
import iconFilter from "../../../assets/images/filter-icon.png";

interface PropsInterface {
  openCloseFilterBar?: () => void;
}

export const HeaderNav: React.FC<PropsInterface> = ({
  openCloseFilterBar = () => {},
}) => {
  const openFilterBar = () => {
    openCloseFilterBar();
  };

  return (
    <>
      <div className="col header">
        <div className="container-fluid p-0">
          <div className="row d-flex justify-content-between">
            <div
              className="col col-filter-nav filter-button d-flex align-items-center pl-4"
              onClick={openFilterBar}
            >
              <img src={iconFilter} alt="filter icon" className="filter-icon" />{" "}
              <span className="ml-2 filter-txt">Filter</span>
            </div>
            <div className="col logo d-flex align-items-center justify-content-center">
              Random<span className="logo-users">Users</span>
            </div>
            <div className="right-side-button">&nbsp;</div>
          </div>
        </div>
      </div>
    </>
  );
};
