import React, { useState, useEffect } from "react";
import { Chip } from "../chip/Chip";
import "./FilterBar.scss";
import iconClose from "../../../assets/images/close-icon.png";

interface PropsInterface {
  addChipName?: (chip: string[]) => void;
  addChipCountry?: (chip: string[]) => void;
  setIsFilteringWholeList?: (value: boolean) => void;
  openCloseFilterBar?: () => void;
}

export const FilterBar: React.FC<PropsInterface> = (props: any) => {
  const [name, setName] = useState("");
  const [country, setCountry] = useState("");
  const [chipsNames, setChipNames] = useState<string[]>([]);
  const [chipsCountries, setChipsCountries] = useState<string[]>([]);

  useEffect(() => {
    props.addChipName(chipsNames);
  }, [chipsNames]);

  useEffect(() => {
    props.addChipCountry(chipsCountries);
  }, [chipsCountries]);

  const handleNameChip = (e: any) => {
    e.target.value !== "" && setChipNames([...chipsNames, e.target.value]);
    props.setIsFilteringWholeList(false);
  };

  const handleCountrieChip = (e: any) => {
    e.target.value !== "" &&
      setChipsCountries([...chipsCountries, e.target.value]);
    props.setIsFilteringWholeList(false);
  };

  const deleteAllChips = () => {
    setChipsCountries([]);
    setChipNames([]);
  };

  const deleteChipName = (chip: string) => {
    props.setIsFilteringWholeList(true);
    setChipNames(chipsNames.filter((name) => name !== chip));
  };
  const deleteChipCountry = (chip: string) => {
    props.setIsFilteringWholeList(true);
    setChipsCountries(chipsCountries.filter((country) => country !== chip));
  };

  return (
    <div className="container-fluid p-0">
      <div className="row close-button-area">
        <div className="col  d-flex align-items-center flex-row-reverse">
          <div
            className="close-button d-flex align-items-center"
            onClick={props.openCloseFilterBar}
          >
            Close{" "}
            <img
              src={iconClose}
              alt="close icon"
              className="ml-2 mr-2 close-icon "
            />{" "}
          </div>
        </div>
      </div>
      <div className="row input-text-fields">
        <div className="col">
          <input
            id="filter-name"
            type="text"
            className="form-control"
            placeholder="Filter by last name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                handleNameChip(e);
                setName("");
              }
            }}
          />
          <input
            id="filter-country"
            type="text"
            className="form-control"
            placeholder="Filter by country"
            value={country}
            onChange={(e) => setCountry(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                handleCountrieChip(e);
                setCountry("");
              }
            }}
          />
          <div className="chips-area">
            {chipsNames.length !== 0 || chipsCountries.length !== 0 ? (
              <Chip
                keyWord="Delete All Filters"
                chipType={"delete"}
                deleteAllChips={deleteAllChips}
              />
            ) : (
              ""
            )}
            {chipsNames.map((name, key) => (
              <Chip
                keyWord={name}
                chipType={"name"}
                key={key}
                deleteChip={deleteChipName}
              />
            ))}
            {chipsCountries.map((country, key) => (
              <Chip
                keyWord={country}
                chipType={"country"}
                key={key}
                deleteChip={deleteChipCountry}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
