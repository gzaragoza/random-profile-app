import React from "react";
import "./Chip.scss";

interface PropsInterface {
  deleteChip?: (chip: string) => void;
  deleteAllChips?: () => void;
  chipType: string;
  keyWord: string;
}

export const Chip: React.FC<PropsInterface> = (props: any) => {
  const deleteChipName = (chip: string) => {
    props.deleteChip(chip);
  };

  return (
    <>
      {props.chipType === "delete" ? (
        <div
          className="chip animated fadeIn"
          style={{
            backgroundColor: "#C4C4C4",
            color: "#000",
            cursor: "pointer",
          }}
          onClick={props.deleteAllChips}
        >
          <div className="chipContent">{props.keyWord}</div>
        </div>
      ) : (
        <div
          className="chip animated fadeIn"
          style={
            props.chipType === "country"
              ? { backgroundColor: "#000", color: "#fff" }
              : { backgroundColor: "#258615", color: "#fff" }
          }
        >
          <div className="chipContent">{props.keyWord}</div>
          <div
            className="ml-2 closeButton"
            onClick={() => deleteChipName(props.keyWord)}
          >
            X
          </div>
        </div>
      )}
    </>
  );
};
