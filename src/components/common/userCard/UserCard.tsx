import React, { useState, useEffect } from "react";
import "./UserCard.scss";
import { User } from "../../../entities/users/users";

export const UserCard = (props: User) => {
  const [markCard, setMarkCard] = useState(false);

  const onShowFilterVar = () => {
    markCard ? setMarkCard(false) : setMarkCard(true);
  };

  useEffect(() => {
    setMarkCard(false);
  }, [props.name.last]);

  return (
    <>
      <div className="usercard" onClick={() => onShowFilterVar()}>
        <div
          className={
            markCard
              ? "usercard__side usercard__side--front usercard__side--front-animate"
              : "usercard__side usercard__side--front"
          }
        >
          <div
            className="usercard__picture"
            style={{ backgroundImage: "url(" + props.picture.large + ")" }}
          >
            &nbsp;
          </div>
          <h4 className="usercard__heading ">
            <span className=" usercard__heading-span-name usercard__heading-span-name--1">
              {props.name.first} {props.name.last}
            </span>
            <span className="usercard__heading-span-country usercard__heading-span-country--1">
              {props.location.country}
            </span>
          </h4>
          <div className="usercard__details"></div>
        </div>
        <div
          className={
            markCard
              ? "usercard__side usercard__side--back usercard__side--back-animate"
              : "usercard__side usercard__side--back"
          }
        >
          <div
            className="usercard__info usercard__picture usercard__picture--back"
            style={{
              backgroundImage:
                "linear-gradient(to right bottom,#268615,#7fc349), url(" +
                props.picture.large +
                ")",
            }}
          >
            <div className="usercard__info-box">
              <p className="usercard__info-name ml-4">
                {props.name.first} {props.name.last}
              </p>
              <p className="usercard__info-details mr-4">
                {props.gender} · {props.dob.age} <br />
                {props.cell}
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
