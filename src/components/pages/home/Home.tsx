import React, { useState, useEffect, useRef } from "react";
import { UserCard } from "../../common/userCard/UserCard";
import { HeaderNav } from "../../common/headerNav/HeaderNav";
import { User } from "../../../entities/users/users";
import axios, { AxiosResponse } from "axios";
import { FilterBar } from "../../common/filterBar/FilterBar";
import "./Home.scss";

export const Home = () => {
  const [randomUsers, setRandomUsers] = useState<User[]>([]);
  const [filteredRandomUsers, setFilteredRandomUsers] = useState<User[]>([]);
  const [chipsSelectedNames, setChipsSelectedNames] = useState<string[]>([]);
  const [chipsSelectedCountries, setChipsSelectedCountries] = useState<
    string[]
  >([]);

  const [filteringWholeList, setFilteringWholeList] = useState(false);
  const [totalNumOfItems, setTotalNumOfItems] = useState(100);
  const [openFilterBar, setOpenFilterBar] = useState(false);

  const getUsers = async () => {
    axios
      .get<User[]>("https://randomuser.me/api/?results=1200")
      .then((response: AxiosResponse) => {
        setRandomUsers(response.data.results);
        setFilteredRandomUsers(response.data.results);
      })
      .catch((ex) => {
        const error =
          ex.response.status === 404
            ? "Resource Not found"
            : "An unexpected error has occurred";
        console.log(error);
      });
  };

  const loader = useRef(null);

  useEffect(() => {
    getUsers();

    const options = {
      root: null,
      rootMargin: "100px",
      threshold: 1.0,
    };
    const boxElement: any = document.querySelector("#loaderId");
    const observer = new IntersectionObserver(handleObserver, options);
    observer.observe(boxElement);
  }, []);

  useEffect(() => {
    filterBy();
  }, [chipsSelectedNames, chipsSelectedCountries]);

  const handleObserver = (entities: any) => {
    const target = entities[0];
    if (target.isIntersecting) {
      setTotalNumOfItems((totalNumOfItems) => totalNumOfItems + 11);
    }
  };

  const addChipName = (chip: string[]) => {
    setChipsSelectedNames(chip);
  };

  const addChipCountry = (chip: string[]) => {
    setChipsSelectedCountries(chip);
  };

  const filterBy = () => {
    let filterItems: User[] = randomUsers;

    if (!filteringWholeList) {
      chipsSelectedNames.forEach(
        (value) =>
          (filterItems = filteredRandomUsers.filter((user) =>
            user.name.last
              .toLocaleLowerCase()
              .includes(value.toLocaleLowerCase())
          ))
      );

      chipsSelectedCountries.forEach(
        (value) =>
          (filterItems = filterItems.filter((user) =>
            user.location.country
              .toLocaleLowerCase()
              .includes(value.toLocaleLowerCase())
          ))
      );
    } else {
      chipsSelectedNames.forEach(
        (value) =>
          (filterItems = randomUsers.filter((user) =>
            user.name.last
              .toLocaleLowerCase()
              .includes(value.toLocaleLowerCase())
          ))
      );
      chipsSelectedCountries.forEach(
        (value) =>
          (filterItems = filterItems.filter((user) =>
            user.location.country
              .toLocaleLowerCase()
              .includes(value.toLocaleLowerCase())
          ))
      );
    }

    chipsSelectedCountries.length === 0 && chipsSelectedNames.length === 0
      ? setFilteredRandomUsers(randomUsers)
      : setFilteredRandomUsers(filterItems);
  };

  const setIsFilteringWholeList = (value: boolean) => {
    setFilteringWholeList(value);
  };

  const openCloseFilterBar = () => {
    openFilterBar ? setOpenFilterBar(false) : setOpenFilterBar(true);
  };

  return (
    <>
      <div className="row no-gutters h100">
        <div className={openFilterBar ? "col-filter-open" : "col-filter"}>
          <FilterBar
            addChipName={addChipName}
            addChipCountry={addChipCountry}
            setIsFilteringWholeList={setIsFilteringWholeList}
            openCloseFilterBar={openCloseFilterBar}
          />
        </div>
        <div className="col">
          <div className="container-fluid">
            <div className="row">
              <HeaderNav openCloseFilterBar={openCloseFilterBar} />
            </div>
            <div className="row">
              <div className="col ">
                <div className="card-deck mt-1">
                  {filteredRandomUsers
                    .slice(0, totalNumOfItems)
                    .map((user, key) => (
                      <UserCard {...user} key={key} />
                    ))}
                </div>
              </div>
            </div>
          </div>
          <div id="loaderId" ref={loader}></div>
        </div>
      </div>
    </>
  );
};
